#include "handler.h"


bool Handler::ProcessFile() {

    std::string fileCountries;
    std::cout << "Input a path to file:" << std::endl;
    std::cin >> fileCountries;

    std::ifstream inputFile(fileCountries);
    if (!inputFile.is_open()) {
        std::cout << "Error while open file: " << fileCountries << std::endl;
        return false;
    }
    
    std::string line;

    while (std::getline(inputFile, line) ) {  
    	std::vector<std::string> data = getLineData(line);
    	
    	if (data.size() == 0) continue;
        
        int nId              = std::stoi(data[0]);
        int nCount           = std::stoi(data[1]);
        std::string sCountry = data[2];
        
        int index = CountryExists(sCountry);

        if (index != -1) {
            UpdateRecord(index, nCount, nId);
        } else {
            InsertRecord(sCountry, nId, nCount);
        }

    }
    
    return true;
}


std::vector<std::string> getLineData(const std::string &line) {

    int state = 0;
    std::vector<std::string> data;

    int signIndexes[2];

    for (int index = 0 ; index < line.length(); index++) {
        switch (state) {
            case 0: {
                if (line[index] == ';') {
                  	signIndexes[0] = index;
                    state = 1;
                } else if (!std::isdigit(line[index])) {
                    return std::vector<std::string>();         
                }
                break;
            }
            case 1: {
                if (line[index] == ';' ) {
                	signIndexes[1] = index;
                    state = 2;
                } else if (!std::isdigit(line[index])) {
                    return std::vector<std::string>();
                }
               	break;     
            }
            case 2: {
                if (!std::isalpha(line[index])) {
                    return std::vector<std::string>(); 
                }
                break;     
            }
        }
    }

    if (state != 2 ) return std::vector<std::string>(); 


    data.push_back(line.substr(0, signIndexes[0]));
    data.push_back(line.substr(signIndexes[0] + 1, signIndexes[1] - signIndexes[0] - 1));
    data.push_back(line.substr(signIndexes[1] + 1, line.length() - signIndexes[1]));

    return data;
}


int Handler::CountryExists(const std::string &sCountry) {

    size_t aCountriesSize = aCountries.size();

    for (size_t i = 0; i < aCountriesSize; i++) {
        if (aCountries[i] == sCountry) return i; 
    }

    return -1;
}


void Handler::UpdateRecord(const int index, const int nCount, const int nId)
{
    aCounts[index] += nCount;

    bool idExists = false;

    for (int id : aIds[index]) {
        if (id == nId) {
            idExists = true;
            break;
        }
    }

    if (!idExists) 
        aIds[index].push_back(nId);
}


void Handler::InsertRecord(const std::string &sCountry, const int nId, const int nCount) {

    aCountries.push_back(sCountry);
    aCounts.push_back(nCount);

    std::vector<int> aIdsTemp;
    aIdsTemp.push_back(nId);
    aIds.push_back(aIdsTemp);
}


void Handler::Output(void) {

    size_t aCountriesSize = aCountries.size();

    for (int i = 0; i < aCountriesSize; i++) {
        std::cout << aCountries[i] << ";" << aCounts[i]<< ";" << aIds[i].size() << std::endl;
    }
}