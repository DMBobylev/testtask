CC = g++
FLAGS = -std=c++11 -O2 -pedantic -Wall -Wno-sign-compare -Wno-long-long -lm
FILES = main.cpp handler.cpp 
PROG = exec
exec:
	$(CC) $(FLAGS) -o $(PROG) $(FILES)
clean:
	rm -f *.o exec