#ifndef HANDLER
#define HANDLER

#include <iostream>
#include <vector>
#include <fstream>

std::vector<std::string> getLineData(const std::string &line);

class Handler
{
    public:
        Handler() {};
        bool ProcessFile(void);
        int CountryExists(const std::string &sCountry);
        void UpdateRecord(const int index, const int nCount, const int nId);
        void InsertRecord(const std::string &sCountry, const int nId, const int nCount);
        void Output(void);
        ~Handler() {};
    private:
        std::vector<std::string>       aCountries;
        std::vector<int>               aCounts;
        std::vector<std::vector<int>>  aIds;
};

#endif